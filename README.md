### Default MongoDB configuration for Bluesky at HZB ###

You can start with the following. 

```
./init_mongodb.sh
```

This will start a mongodb using mongo version 6.

There will be an admin database called `test` with the user `madmin` and password `madminpw`. These are set in the environment variables in the `init_mongodb.sh` file which is used to start the service. 

When the mongodb is first started the `db_scripts/mongo-init.js` script will be run. You can look in the file and see that it will create another database called `bluesky` with user `blueskyuser` and password `sun`

`init_mongodb.sh` mounts a drive on the host machine so that the database is persisted even if it's stopped and restarted

#### Migrating an Existing mongodb ####

You can migrate an existing mongoDB by first exporting a dump from the existing database

`mongodump`

Then copy that dump into this running container

`docker cp /path/to/dump CONTAINER:/tmp/dump`

Then attach to the running container and use use mongorestore on the dump

`mongorestore /tmp/dump`

