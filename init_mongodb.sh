#!/bin/bash
################################################################################
#
#
################################################################################

MDATAPATH=$PWD
DBNAME=bluesky

docker run -d --restart=always --net=host --name mongoDB-for-bluesky \
	-e MONGO_INITDB_ROOT_USERNAME=madmin \
	-e MONGO_INITDB_ROOT_PASSWORD=madminpw \
    -e MONGO_INITDB_DATABASE=$DBNAME \
    --volume $MDATAPATH/data:/data/db \
    --volume $MDATAPATH/db_scripts/mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro \
	mongo:6.0.4
